

App = {
  
	init: function() {
	  // bind button events
	  App.bindEvents();
	},
  
	trim: function (str){ 
	 return str.replace(/(^\s*)|(\s*$)/g, ""); 
 	},
  
	bindEvents: function() {
	  $('.repo_h').on('click', App.handleSelectRepo);
	  $('#btn_detect').on('click',  App.handleDetect);
	// 	$(document).on('click', '#btn_submit', App.confirmAdd);

	},
  

	handleSelectRepo: function(event) {
		event.preventDefault();
		var repo = $(this).text();
		$("#repo_s").text(repo);
		$("#repo_s").attr("repo-id",$(this).attr("repo-id"));
	},

	handleDetect: function(event) {
		event.preventDefault();
		// verify the value of #query
		$('#pr_rows').html("")
		$('#exa_pr').attr("style","display:none");
		$('#resultRow').attr("style","display:none");
		var pr_n = $("#repo_n").val().trim()
		if (pr_n == ""){
			alert("please input the pr number");
			// $("#repo_n").focus();
			return false;
		}

		var params = {
			'repo_id': $("#repo_s").attr("repo-id"),
			'pr_n':pr_n
		};

		$.ajax({
			data: params,
			url: '/detect',
			type: 'get',
			dataType: 'json',
			cache: false,
			timeout: 5000,
			success: function(data){
				if(data["success"] == "false"){
					alert("pr does not exits, please examine another one!");
					return false;
				}

				// 设置被观测的pr的样式
				var exa_pr = data.exa_pr
				var exa_pr_view = $('#exa_pr')
				exa_pr_view.attr("style","display:block");
				exa_pr_view.find('#pr_author').text(exa_pr.pr_author);
				exa_pr_view.find('#pr_time').text(exa_pr.pr_time);
				exa_pr_view.find('#pr_number').text(exa_pr.pr_number);
				exa_pr_view.find('#pr_title').text(exa_pr.pr_title);

				exa_pr_view.find('#pr_desc').text(exa_pr.pr_desc);
				matches =exa_pr.pr_desc.match(/\n/g);
				breaks = matches == null ? 0 : (matches.length>3?3:matches.length);
				exa_pr_view.find('#pr_desc').attr('rows',breaks+1);

				exa_pr_view.find('#pr_file').text(exa_pr.pr_file);
				matches =exa_pr.pr_file.match(/\n/g);
				breaks = matches == null ? 0 : (matches.length>3?3:matches.length);
				exa_pr_view.find('#pr_file').attr('rows',breaks+1);

				// 设置相似pr的样式
				$('#resultRow').attr("style","display:block");
				var prRow = $('#pr_rows');
				var prTemplate = $('#pr_template');
				data = data.prs;
				// prRow.append(prTemplate.clone(true).attr("class","firstli").html("<h3>Similar PRs</h3>"));

				for (i = 0; i < data.length; i ++) {
					prTemplate.find('#pr_author').text(data[i].pr_author);
					prTemplate.find('#pr_time').text(data[i].pr_time);
					prTemplate.find('#pr_number').text(data[i].pr_number);
					prTemplate.find('#pr_title').text(data[i].pr_title);

					prTemplate.find('#pr_desc').text(data[i].pr_desc);
					matches = data[i].pr_desc.match(/\n/g);
					breaks = matches == null ? 0 : (matches.length>3?3:matches.length);
					prTemplate.find('#pr_desc').attr('rows',breaks+1);

					prTemplate.find('#pr_file').text(data[i].pr_file);
					matches = data[i].pr_file.match(/\n/g);
					breaks = matches == null ? 0 : (matches.length>3?3:matches.length);
					prTemplate.find('#pr_file').attr('rows',breaks+1);

					prTemplate.find('#btn_yes').attr('pr_n',data[i].pr_number);
					prTemplate.find('#btn_no').attr('pr_n',data[i].pr_number);


					new_pr = prTemplate.clone(true)
					if (i == data.length-1){
						new_pr.attr("style","border-bottom: 0px solid #cccccc;")
					}
					prRow.append(new_pr);
				}



			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('error ' + textStatus + " " + errorThrown);  
			}
		}); 

	},
  
  
};
  
  $(function() {
	$(window).load(function() {
	  App.init();
	});
  });
  